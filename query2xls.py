#!/usr/bin/env python3

import argparse
import MySQLdb
import os
import xlwt

def host_connect(host, username, password, port):
    """ try to connect to host specified
    return None if it fails or a Connection object if it connects
    """
    db = None
    try:
        db = MySQLdb.connect(host=host, user=username, passwd=password, port=port)
    except MySQLdb.Error as e:
        # TODO: return code error
        pass

    return db


def db_connect(connection, database):
    """ try to connect to database """
    cursor = connection.cursor()
    try:
        cursor.execute("USE {}".format(database))
    except MySQLdb.Error as e:	
        # TODO: return code error ?	
        return False

    return True


def generate_xls(filename, values, fields=None):
    """ generate xls file with name filename,
    header with fields an rows with values
    """	
    if not len(values):
        return

    wb = xlwt.Workbook()
    ws = wb.add_sheet('Query', cell_overwrite_ok=True)
    styleBold = xlwt.easyxf('font: bold on')

    r = 1
    c = 1

    # write header
    if fields:
        for f in fields:
            ws.write(r, c, f, styleBold)
            c += 1

        r += 1

    c = 1

    # write data
    for row in values:
        for cell in row:
            ws.write(r, c, cell)
            c += 1

        c = 1
        r += 1

    wb.save(get_valid_path(filename))


def get_valid_path(filename):
    """ get a nonexistent name to avoid overwriting a file with the same name
    """
    filename = os.path.expanduser(filename) # prevent '~'

    if not os.path.exists(filename):
        return filename

    # ex: filename: '/home/user/myfile' , extension: '.txt'
    filename, extension = os.path.splitext(filename)
    i = 1
    new_filename = '{}-{}{}'.format(filename, i, extension)
    while os.path.exists(new_filename):
        i += 1
        new_filename = '{}-{}{}'.format(filename, i, extension)

    return new_filename		


def main():
    parser = argparse.ArgumentParser()

    # positional arguments
    parser.add_argument('query', help='sql statement to get the data to be exported')
    parser.add_argument('filename', help='the file where the data will be exported')

    # optional arguments
    parser.add_argument('--host', help='host to connect to', default='localhost')
    parser.add_argument('--username', help='the user name to use when connecting to the server')
    parser.add_argument('--password', help='the password to use when connecting to the server')
    parser.add_argument('--database', help='the database to use')
    parser.add_argument('--port', help='the tcp/ip port number to use for the connection', 
        type=int, default=3306)

    args = parser.parse_args()

    conn = host_connect(args.host, args.username, args.password, args.port)

    if not conn:
        sys.exit("Error connecting to host!")

    if not db_connect(conn, args.database):
        sys.exit("Error connecting to selected database!")

    cursor = conn.cursor()
    cursor.execute(args.query)

    desc = cursor.description
    # FIXME: use list comprehensions
    fields = []
    for i in desc:
        fields.append(i[0])

    values = cursor.fetchall()

    generate_xls(args.filename, values, fields)
    print("xls generated!")

    cursor.close()
    conn.close()


if __name__ == "__main__":
    main()