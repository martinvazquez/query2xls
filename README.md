## query2xls

Simple script to export a mysql query to an xls file

---

**Examples**

./query2xls "select * from user" Users.xls --host=mysite.net --user=martin --pass=123456 --database=blog --port=3366

with defaults arguments: host & port  
./query2xls "select * from user" Users.xls --user=martin --pass=123456 --database=blog 
